package br.eti.arthurgregorio.sistemarquivos;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FilenameUtils;

public class FileReader {

    public void read(Path path) {
        try {
            String extension = FilenameUtils.getExtension(path.toString());
            String content = Files.readString(path);
            if(!extension.equalsIgnoreCase("txt")){
                throw new UnsupportedOperationException("Não há suporte ao arquivo");
            }
            System.out.println(content);
      } catch (IOException e) {
            throw new UnsupportedOperationException("Arquivo não encontrado!");
        }
    }

    public Path open(Path path, String diretorio){
        Set<String> conteudo;
        conteudo = Stream.of(new File(path.toString()).listFiles())
                .filter(file -> file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toSet());

        for(String pasta : conteudo){
            if(pasta.equalsIgnoreCase(diretorio)){
                return Path.of(path + File.separator + diretorio);
            }
        }
        throw new UnsupportedOperationException("Diretório não encontrado");
    }
}
