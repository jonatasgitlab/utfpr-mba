package br.eti.arthurgregorio.sistemarquivos;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Command {

    LIST() {
        @Override
        boolean accept(String command) {
            final var commands = command.split(" ");
            return commands.length > 0 && commands[0].startsWith("LIST") || commands[0].startsWith("list");
        }

        @Override
        Path execute(Path path) throws IOException {
           Set<String> conteudo;
           conteudo = Stream.of(new File(path.toString()).listFiles())
                    //.filter(file -> !file.isDirectory()
                    .map(File::getName)
                    .collect(Collectors.toSet());
            System.out.println("[Conteúdo do diretório " + path + "]");
            for (String s : conteudo){
                System.out.println(s);
            }
            return path;
        }
    },
    SHOW() {
        private String[] parameters = new String[]{};

        @Override
        void setParameters(String[] parameters) {
            this.parameters = parameters;
        }

        @Override
        boolean accept(String command) {
            final var commands = command.split(" ");
            return commands.length > 0 && commands[0].startsWith("SHOW") || commands[0].startsWith("show");
        }

        @Override
        Path execute(Path path) {
            FileReader reader = new FileReader();
            final Scanner scanner = new Scanner(System.in);
            System.out.println("Informe o nome do arquivo: ");
            Path pathCompleto = Path.of(path + File.separator + scanner.nextLine());

            try{
                reader.read(pathCompleto);
            } catch (UnsupportedOperationException e){
                System.out.println(e.getMessage());
            }


            return path;
        }
    },
    BACK() {
        @Override
        boolean accept(String command) {
            final var commands = command.split(" ");
            return commands.length > 0 && commands[0].startsWith("BACK") || commands[0].startsWith("back");
        }

        @Override
        Path execute(Path path) {
            if(path.equals(path.getRoot())){
                System.out.println("Não foi possível voltar um nível de diretório. Já estamos na raiz!");
                return path;
            } else {
                System.out.println("Voltando um nível de diretório...");
                return path.getParent();
            }
        }
    },
    OPEN() {
        private String[] parameters = new String[]{};

        @Override
        void setParameters(String[] parameters) {
            this.parameters = parameters;
        }

        @Override
        boolean accept(String command) {
            final var commands = command.split(" ");
            return commands.length > 0 && commands[0].startsWith("OPEN") || commands[0].startsWith("open");
        }

        @Override
        Path execute(Path path) {
            FileReader reader = new FileReader();
            final Scanner scanner = new Scanner(System.in);
            System.out.println("Informe o nome do diretório: ");
            try {
                Path novoPath = reader.open(path, scanner.nextLine());
                System.out.println("Navegando para " + novoPath);
                return novoPath;
            } catch (UnsupportedOperationException e) {
                System.out.println(e.getMessage());
            }
            return path;
        }
    },
    DETAIL() {
        private String[] parameters = new String[]{};

        @Override
        void setParameters(String[] parameters) {
            this.parameters = parameters;
        }

        @Override
        boolean accept(String command) {
            final var commands = command.split(" ");
            return commands.length > 0 && commands[0].startsWith("DETAIL") || commands[0].startsWith("detail");
        }

        @Override
        Path execute(Path path) throws IOException {
            FileReader reader = new FileReader();
            final Scanner scanner = new Scanner(System.in);
            System.out.println("Informe o nome do aquivo ou diretório que deseja detalhar: ");
            BasicFileAttributes attr = Files.getFileAttributeView(Path.of(path + File.separator + scanner.nextLine()), BasicFileAttributeView.class).readAttributes();
            System.out.println("É um diretório: " + attr.isDirectory());
            System.out.println("Horário de criação: " + attr.creationTime());
            System.out.println("Tamanho: " + attr.size());
            System.out.println("Útima modificação: " + attr.lastModifiedTime());
            return path;
        }
    },
    EXIT() {
        @Override
        boolean accept(String command) {
            final var commands = command.split(" ");
            return commands.length > 0 && commands[0].startsWith("EXIT") || commands[0].startsWith("exit");
        }

        @Override
        Path execute(Path path) {
            System.out.print("Saindo...");
            return path;
        }

        @Override
        boolean shouldStop() {
            return true;
        }
    };

    abstract Path execute(Path path) throws IOException;

    abstract boolean accept(String command);

    void setParameters(String[] parameters) {
    }

    boolean shouldStop() {
        return false;
    }

    public static Command parseCommand(String commandToParse) {

        if (commandToParse.isBlank()) {
            throw new UnsupportedOperationException("Type something...");
        }

        final var possibleCommands = values();

        for (Command possibleCommand : possibleCommands) {
            if (possibleCommand.accept(commandToParse)) {
                possibleCommand.setParameters(commandToParse.split(" "));
                return possibleCommand;
            }
        }

        throw new UnsupportedOperationException("Can't parse command [%s]".formatted(commandToParse));
    }
}
