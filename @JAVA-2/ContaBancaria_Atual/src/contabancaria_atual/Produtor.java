/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package contabancaria_atual;

import java.text.DecimalFormat;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Produtor extends Thread {

    private Estoque estoque;
    private JTextArea jta;
   
    static DecimalFormat df = new DecimalFormat("##0.00");
    public Produtor() {
    }

    public Produtor(Estoque estoque,JTextArea jta) {
        this.estoque = estoque;
        this.jta= jta;
        
    }

    public void produzir() {

        synchronized (estoque) {
            Double aux = 100 + (Math.random() * 235.0);
            String temp= df.format(aux);
            aux = Double.parseDouble(temp.replace(",","."));
            /* Insere um recurso no estoque */
            
            if (this.estoque.getConteudo() == null){
                this.estoque.conteudo = aux;
                System.out.println("+ " + this.getName() + "\t -> Recurso produzido: " + this.estoque.getConteudo());
                this.jta.setText(this.jta.getText()+"Joao Depositou: R$"+aux+ "\n"+ "\t Saldo: "+df.format(this.estoque.conteudo)+"\n");

            }else if((estoque.conteudo + aux) < 1000) {
                if (this.estoque.getConteudo() != null) {
                    this.estoque.conteudo = this.estoque.getConteudo() + aux;
                    System.out.println("+ " + this.getName() + "\t -> Recurso produzido: " + this.estoque.getConteudo());
                    this.jta.setText(this.jta.getText()+"Joao Depositou: R$"+aux+ "\n"+ "\t Saldo: "+df.format(this.estoque.conteudo)+"\n");
                    
                } else {
                    this.estoque.conteudo = aux;
                    System.out.println("+ " + this.getName() + "\t -> Recurso produzido: " + this.estoque.getConteudo());
                    this.jta.setText(this.jta.getText()+"Joao Depositou: R$"+aux+ "\n"+ "\t Saldo: "+df.format(this.estoque.conteudo) +"\n");
                }

            } else {
                /* Não existe recursos no estoque */
                try {
                    System.out.println("! " + this.getName() + "\t -> Produtor esperando estoque ser retirado...");
                    this.jta.setText(this.jta.getText()+ "! " + this.getName() + "\t -> Joao tentou e nao conseguiu(R$"+ aux +")\n");
                    /* Espera o produtor notificar que houve uma reposição no estoque */
                    estoque.wait((int)5000);
                    
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            /* Notifica os consumidores que o estoque já foi atualizado */
            estoque.notifyAll();
        }
    }

    public void run() {

        while (true) {

            this.produzir();

            try {
                Thread.sleep((int) (Math.random() * Configuracoes.MAX_TIME_TO_SLEEP));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public Estoque getEstoque() {
        return estoque;
    }

    public void setEstoque(Estoque estoque) {
        this.estoque = estoque;
    }
}
