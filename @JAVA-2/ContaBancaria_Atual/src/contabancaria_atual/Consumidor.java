/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package contabancaria_atual;

import java.text.DecimalFormat;
import javax.swing.JTextArea;
import javax.swing.JTextField;

 public class Consumidor extends Thread {

    private Estoque estoque;
    private JTextArea jta;
   
    static DecimalFormat df = new DecimalFormat("##0.00");
    public Consumidor() { }

    public Consumidor(Estoque estoque,JTextArea jta) {
       this.estoque = estoque;
       this.jta= jta;
       

    }


    public void consumir() {

       synchronized (estoque) {
            Double aux =1.0 + (Math.random() * 16.5);
            String temp= df.format(aux);
            aux = Double.parseDouble(temp.replace(",","."));
          /* Verifica se existem itens no estoque */
          if (estoque.conteudo-aux >= 0) {
             this.estoque.conteudo -= aux;
             System.out.println("- " + this.getName() + "\t -> Recurso consumido: " + this.estoque.conteudo);
             //this.jb.setText(aux.toString());
              this.jta.setText(this.jta.getText()+"Fulano sacou: R$"+aux+ "\n" + "\t Saldo: "+ df.format(this.estoque.conteudo)+"\n");
              
          }
          else {
             /* Não existe recursos no estoque */
             try {
                System.out.println("! " + this.getName() +  "\t -> Não pode retirar nada");

                /* Espera o produtor notificar que houve uma reposição no estoque */
                estoque.wait(5000);
                
             }
             catch (InterruptedException e) {
                e.printStackTrace();
             }
          }
       }
    }

    public void run() {
       while (true) {

          this.consumir();

          try {
             Thread.sleep((int)(Math.random() * Configuracoes.MAX_TIME_TO_SLEEP));
          }
          catch (InterruptedException e) {
             e.printStackTrace();
          }
       }
    }


    public Estoque getEstoque() {
       return estoque;
    }

    public void setEstoque(Estoque estoque) {
       this.estoque = estoque;
    }
 }
