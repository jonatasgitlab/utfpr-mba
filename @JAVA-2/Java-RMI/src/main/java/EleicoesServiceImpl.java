import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;


public class EleicoesServiceImpl implements EleicoesService{

    List<Candidato> candidatos = new ArrayList<>();

    @Override
    public boolean receberVoto(String nomeCandidato, Integer numVoto) throws RemoteException {
        System.out.println("Começou Receber voto");
        //Recebendo primeito Voto
        Optional<Candidato> votoRecebido = candidatos.stream()
                .filter(candidato -> candidato.getNome().equals(nomeCandidato))
                .findFirst();

        //Verificar se candidato não esta na lista dos votados
        if(votoRecebido.isPresent() && votoRecebido.get().getNome().equals("")){
            if(!nomeCandidato.equals("") && !numVoto.equals("")){
                Candidato candidato = new Candidato();
                candidato.setNome(nomeCandidato);
                candidato.setNumVotos(1);

                System.out.println("Primeiro voto para o candidato: "  + candidato.getNome());
                candidatos.add(candidato);
                return true;
            }
        } else {
            for(Candidato c : candidatos){
                if(c.getNome().equals(nomeCandidato)){
                    System.out.println("Novo voto para o candidato: " + c.getNome());
                    c.setNumVotos(c.getNumVotos()+1);
                }
            }
            return true;
        }

        return false;
    }

    @Override
    public List<Candidato> contarVotos() throws RemoteException {
        return null;
    }

    public static void main(String[] args) throws RemoteException {
        EleicoesServiceImpl eleicoesService = new EleicoesServiceImpl();

        EleicoesService stub = (EleicoesService) UnicastRemoteObject.exportObject(eleicoesService, 0);

        Registry registro = LocateRegistry.createRegistry(22);
        try {
            registro.bind("metodosEleicoes", stub);
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
        }

        System.out.println("Servidor RMI, OK!!!");
    }

}
