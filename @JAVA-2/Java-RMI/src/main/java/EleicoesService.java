import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface EleicoesService extends Remote {
    boolean receberVoto(String nomeCandidato, Integer numVoto) throws RemoteException;
    List<Candidato> contarVotos() throws RemoteException;
}
