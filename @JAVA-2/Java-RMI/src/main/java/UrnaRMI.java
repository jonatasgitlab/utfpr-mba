import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class UrnaRMI {
    public static void main(String[] args) throws RemoteException {
        Registry registro = LocateRegistry.getRegistry(22);

        EleicoesService stub = null;
        try {
            stub = (EleicoesService) registro.lookup("metodosEleicoes");
        } catch (NotBoundException e) {
            e.printStackTrace();
        }

        boolean repostaVoto = stub.receberVoto("Eneias 5656", 1);
        System.out.println("Voto Realizado " + repostaVoto);

        stub.contarVotos();
    }
}
