package br.eti.arthurgregorio.contadorvendas;

import com.opencsv.bean.CsvToBeanBuilder;

import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SalesReader {

    private final List<Sale> sales;
    Stream<Sale> streamSales;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public SalesReader(String salesFile) {

        final var dataStream = ClassLoader.getSystemResourceAsStream(salesFile);

        if (dataStream == null) {
            throw new IllegalStateException("File not found or is empty");
        }

        final var builder = new CsvToBeanBuilder<Sale>(new InputStreamReader(dataStream, StandardCharsets.ISO_8859_1));

        sales = builder
                .withType(Sale.class)
                .withSeparator(';')
                .build()
                .parse();
        streamSales = sales.stream();
    }

    public void totalCompletedSales() {

        BigDecimal totalVendasConluidas = sales.stream()
                .filter(venda -> venda.isCompleted())
                .map(venda -> venda.getValue())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("total (em R$) de vendas completas = R$" + toCurrency(totalVendasConluidas));

    }



    public void totalCancelledSales() {

        BigDecimal totalVendasCanceladas = sales.stream()
                .filter(venda -> venda.isCancelled())
                .map(venda -> venda.getValue())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("total (em R$) de vendas canceladas = R$" + toCurrency(totalVendasCanceladas));
    }

    public void mostRecentSale() {
        LocalDate menorData = LocalDate.now();

        menorData = sales.stream()
                .filter(venda -> venda.getSaleDate().isBefore(LocalDate.now()))
                .map(venda -> venda.getSaleDate())
                .min(LocalDate::compareTo)
                .get();

        System.out.println("Data da Primeira Venda = " + menorData.format(formatter));
    }

    public void daysBetweenFirstAndLast() {
        LocalDate menorData = LocalDate.now();

        menorData = sales.stream()
                .filter(venda -> venda.getSaleDate().isBefore(LocalDate.now()))
                .map(venda -> venda.getSaleDate())
                .min(LocalDate::compareTo)
                .get();
        LocalDate maiorData = menorData;
        LocalDate finalMaiorData = maiorData;

        maiorData = sales.stream()
                .filter(venda -> venda.getSaleDate().isAfter(finalMaiorData))
                .map(venda -> venda.getSaleDate())
                .max(LocalDate::compareTo)
                .get();

        long dias = ChronoUnit.DAYS.between(menorData, maiorData);
        System.out.println("A quantidade de dias entre a primeira e a ultima venda = " + dias);
    }

    public void totalSalesBySeller(String sellerName) {
        // TODO encontrar o total (em R$) de vendas do vendedor recebido por parametro

        BigDecimal totalVendedor = sales.stream()
                .filter(venda -> venda.getSeller().equals(sellerName))
                .map(venda -> venda.getValue())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("o total (em R$) de vendas do vendedor " + sellerName + " foi = R$" + toCurrency(totalVendedor));
    }

    public void countSalesByManager(String managerName) {
        int totalVendas = sales.stream()
                .filter(venda -> venda.getManager().equals(managerName))
                .map(venda -> 1)
                .reduce(0, Integer::sum);
        System.out.println("quantidade de vendas para o gerente " + managerName + " foi = " + totalVendas);
    }

    public void totalSalesByMonth(Month... months) {
        BigDecimal totalVendas = sales.stream()
                .filter(venda -> Arrays.stream(months).anyMatch(mes -> mes.equals(venda.getSaleDate().getMonth())))
                .map(Sale::getValue)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("o valor (em R$) de vendas para os meses " + Arrays.stream(months).toList() + " R$" + toCurrency(totalVendas));
    }

    public void rankingByDepartment() {

        Map<String, Long> ranking = sales.stream()
                .collect(Collectors.groupingBy(venda -> venda.getDepartment(), Collectors.counting()));

        System.out.println("\nRanking contando o total (quantidade) de vendas por departamento");

        ranking.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEach(System.out::println);
    }

    public void rankingByPaymentMethod() {
        // TODO faca um ranking contando o total (quantidade) de vendas por meio de pagamento
        Map<String, Long> ranking = sales.stream()
                .collect(Collectors.groupingBy(venda -> venda.getPaymentMethod(), Collectors.counting()));

        System.out.println("\nRanking contando o total (quantidade) de vendas por meio de pagamento");

        ranking.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEach(System.out::println);

    }

    public void bestSellers() {
        // TODO faca um top 3 dos vendedores que mais venderam (ranking por valor em vendas)
        Map<String, Long> ranking  = sales.stream()
                .collect(Collectors.groupingBy(venda -> venda.getSeller(), Collectors.counting()));
        System.out.println("\nRanking vendedores que mais venderam (ranking por valor em vendas)");
        ranking.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEach(System.out::println);

        Map<String, List<Sale>> vendas = sales.stream()
                .collect(Collectors.groupingBy(venda -> venda.getSeller()));

        vendas.entrySet()
                .stream()
                .map(venda -> venda.getValue()
                        .stream()
                        .map(vend -> vend.getValue())
                        .reduce(BigDecimal.ZERO, BigDecimal::add))
                .forEach(bigDecimal -> System.out.println(bigDecimal));

        System.out.println();
    }

    /*
     * Use esse metodo para converter objetos BigDecimal para uma represetancao de moeda
     */
    private String toCurrency(BigDecimal value) {
        return NumberFormat.getInstance().format(value);
    }

    public class Ranking{
        private String chave;
        private Integer valor;
    }
}
