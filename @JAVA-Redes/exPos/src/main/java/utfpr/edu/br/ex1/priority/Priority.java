package utfpr.edu.br.ex1.priority;

class Priority implements Runnable {
	int count;
	Thread myThrd;

	static boolean stop = false;
	static String currentName;

	Priority(String name) {
		myThrd = new Thread(this, name);
		count = 0;
		currentName = name;
	}
	public void run() {
		System.out.println(myThrd.getName() + " starting.");

		do {
			count++;
			if(currentName.compareTo(myThrd.getName()) != 0) {
				currentName = myThrd.getName();
				System.out.println("In " + currentName);
			}
		} while(stop == false && count < 10000000);
	stop = true;
	System.out.println("\n" + myThrd.getName() + " terminating.");
	}
}