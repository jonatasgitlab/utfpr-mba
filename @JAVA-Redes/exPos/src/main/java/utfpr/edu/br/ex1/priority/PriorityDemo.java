package utfpr.edu.br.ex1.priority;

class PriorityDemo {
	public static void main(String args[]) {
		Priority mt1 = new Priority("High Priority");
		Priority mt2 = new Priority("Low Priority");

		mt1.myThrd.setPriority(Thread.NORM_PRIORITY+2);
		mt2.myThrd.setPriority(Thread.NORM_PRIORITY-2);

		mt1.myThrd.start();
		mt2.myThrd.start();

		try {
			mt1.myThrd.join();
			mt2.myThrd.join();
		}
		catch(InterruptedException exc) {
			System.out.println("Main thread interrupted.");
		}	
		System.out.println("\nHigh priority thread counted to " + mt1.count);
		System.out.println("Low priority thread counted to " + mt2.count);
	}
}
