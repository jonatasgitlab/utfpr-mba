package utfpr.edu.br.ex1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CorridaMoto {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Campeonato Iniciando!");
        ArrayList<Corredor> classificacao = new ArrayList<>();
        for(int i=1; i < 11; i++) {
            System.out.println("------------------- Iniciando a corrida " + i);
            Thread.sleep(2000);
            Corredor c1 = new Corredor("#1");
            Corredor c2 = new Corredor("#2");
            Corredor c3 = new Corredor("#3");
            Corredor c4 = new Corredor("#4");
            Corredor c5 = new Corredor("#5");
            Corredor c6 = new Corredor("#6");
            Corredor c7 = new Corredor("#7");
            Corredor c8 = new Corredor("#8");
            Corredor c9 = new Corredor("#9");
            Corredor c10 = new Corredor("#10");
            try {
                c1.thrd.join();
                c2.thrd.join();
                c3.thrd.join();
                c4.thrd.join();
                c5.thrd.join();
                c6.thrd.join();
                c7.thrd.join();
                c8.thrd.join();
                c9.thrd.join();
                c10.thrd.join();

                ArrayList<Corredor> corredores = new ArrayList<>();
                corredores.add(c1);
                corredores.add(c2);
                corredores.add(c3);
                corredores.add(c4);
                corredores.add(c5);
                corredores.add(c6);
                corredores.add(c7);
                corredores.add(c8);
                corredores.add(c9);
                corredores.add(c10);


                classificacao = calcularPontuacao(corredores, classificacao);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Collections.sort(classificacao, Comparator.comparing(Corredor::getPts));
        Collections.reverse(classificacao);
        classificacao.get(0).nome  = classificacao.get(0).nome + " FOI O VENCEDOR ";
        System.out.println(">>>>>>>>>>>>> FIM DO CAMPEONATO <<<<<<<<<<<<<");
        for(Corredor c : classificacao){
            System.out.println("Corredor " + c.nome + " terminou o campeonado com "  + c.pts + " pontos!!");
        }
    }

    private static class Corredor implements Runnable{
        Thread thrd;
        Integer pts;
        String nome;
        Long resultado;
        Corrida corrida = new Corrida();

        public Corredor(String nome) {
            thrd = new Thread(this, nome);
            pts = 0;
            this.nome = nome;
            thrd.start();
        }

        Long getResultado(){
            return this.resultado;
        }

        Integer getPts(){
            return this.pts;
        }

        @Override
        public void run() {
           synchronized (corrida){
               resultado = corrida.etapa();
           }
        }
    }

    private static   class Corrida {
    long start = 0;

        public Long etapa(){
            try {
                start = System.currentTimeMillis();
                for(int count=0; count < 10; count++) {
                    Thread.sleep((long)(Math.random() * 1000));
                    System.out.println("Corredor " + Thread.currentThread().getName() + ", passou pela curva " + count);
                }
            } catch(InterruptedException exc) {
                System.out.println(Thread.currentThread().getName() + " interrupted.");
            }
            long finish = System.currentTimeMillis();
            long tempoTermino = (finish - start);
            System.out.println("Corredor " + Thread.currentThread().getName() + " cruzou a linha de chegada em " + Double.parseDouble(String.valueOf(tempoTermino))/1000 + " segundos!");

            return tempoTermino;
        }

    }

    private static ArrayList<Corredor> calcularPontuacao(ArrayList<Corredor> corredores, ArrayList<Corredor> classificacao){
    //Ordenando por ordem de chegada
        Collections.sort(corredores, Comparator.comparing(Corredor::getResultado));

        Integer i = 10;
        for(Corredor c : corredores){
            c.pts = c.pts + i;
            System.out.println("Corredor " + c.nome + " recebeu " + c.pts + " pontos");
            i--;
            if(classificacao.size() > 0 ){
                for(Corredor classi : classificacao){
                    if(c.nome.equals(classi.nome)){
                       classi.pts = classi.pts + c.pts;
                    }
                }
            }
        }
        if(classificacao.size() == 0 ){
           return corredores;
        }

        return classificacao;
    }

}
