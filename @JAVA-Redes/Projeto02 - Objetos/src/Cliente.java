
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Cliente {

    private static Socket socket;
    private static DataInputStream entrada;
    private static ObjectOutputStream saida;

    public static void main(String[] args) {

        try {


            String titulo = "Cliente x Servidor";
            JFrame jan1 = new JFrame(titulo);
            JLabel lblNome = new JLabel("Nome");
            JLabel lblidade = new JLabel("Idade");
            JLabel lblRet = new JLabel("Retorno Servidor");
            JTextField txtNome = new JTextField("      ");
            JTextField txtIdade = new JTextField("      ");
            JTextArea txtRet  = new JTextArea("  ");
            JButton btnEnviar = new JButton("Enviar");


            int larg = 500, alt = 200;
            jan1.setSize(larg, alt); // redimensionar: largura e depois altura
            jan1.setTitle(titulo); // trocar o título inicial
            // “EXIT_ON_CLOSE”: fecha a aplicação toda.
            jan1.setDefaultCloseOperation(jan1.EXIT_ON_CLOSE);

            jan1.add(lblNome);
            jan1.add(txtNome);
            jan1.add(lblidade);
            jan1.add(txtIdade);
            jan1.add(btnEnviar);
            jan1.add(lblRet);
            jan1.add(txtRet);

            jan1.setLayout(new FlowLayout());

            Pessoa p = new Pessoa();
            // CLIQUE PASSEIO
            btnEnviar.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    p.setNome(txtNome.getText());
                    p.setIdade(Integer.parseInt(txtIdade.getText().replaceAll("[^0-9.]", "")));
                    try {
                        socket = new Socket("127.0.0.1", 50000);
                        saida = new ObjectOutputStream(socket.getOutputStream());
                        entrada = new DataInputStream(socket.getInputStream());
                        saida.writeObject(p);
                        txtRet.setText(entrada.readUTF());
                        socket.close();

                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }

            });
            jan1.setVisible(true);


        } catch(Exception e) {

        }

    }

}
