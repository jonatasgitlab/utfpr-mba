
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {
    
    private static Socket socket;
    private static ServerSocket servidor;

    public static void main(String[] args) {
   
        try {

            try {
                // receber mensagens
                servidor = new ServerSocket(50000);
                while (true) {
                    System.out.println("Aguardando conexão...");
                    socket = servidor.accept();
                    System.out.println("Conexão estabelecida...");
                    // regra de negocio
                    new Thread(new ThreadServidor(socket)).start();
                }
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
                "Dados recebidos corretamente!");
            
        } catch(Exception e) {
            
        }
        
    }
    
}
