import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreadServidor implements Runnable{

    private Socket conexao;
    private ObjectInputStream entrada;
    private DataOutputStream saida;

    public ThreadServidor(Socket c){
        this.conexao = c;
    }

    @Override
    public void run() {
        try {
            // executar o servico
            //entrada = new DataInputStream(conexao.getInputStream());
            entrada = new ObjectInputStream(conexao.getInputStream());
            Pessoa p = (Pessoa) entrada.readObject();

            System.out.println("Nome: " + p.getNome() + " Idade: " + p.getIdade());

            saida = new DataOutputStream(conexao.getOutputStream());
            saida.writeUTF("Recebeu do servidor: " +
                    "Dados recebidos corretamente!");

            // finalizar conexao
            conexao.close();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ThreadServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
