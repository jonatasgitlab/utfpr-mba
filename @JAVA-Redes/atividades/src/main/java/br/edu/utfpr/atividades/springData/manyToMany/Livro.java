package br.edu.utfpr.atividades.springData.manyToMany;

import br.edu.utfpr.atividades.springData.manyToOne.Cliente;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "LIVROS")
public class Livro {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "nome", length = 64, nullable = false)
    private String nome;

    @ManyToMany()
    @JoinTable(name = "livro_autor",
            joinColumns = @JoinColumn(name = "livro_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "autor_id",
                    referencedColumnName = "id"))
    private List<Autor> autores;
}
