package br.edu.utfpr.atividades.springData.manyToOne;

import br.edu.utfpr.atividades.springData.oneToMany.Funcionario;
import org.hibernate.mapping.Join;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PEDIDOS")
public class Pedido {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "nome", length = 64, nullable = false)
    private String nome;

    @ManyToOne()
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;
}
