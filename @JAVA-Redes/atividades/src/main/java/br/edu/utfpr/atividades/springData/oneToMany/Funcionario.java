package br.edu.utfpr.atividades.springData.oneToMany;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "FUNCIONARIOS")
public class Funcionario {

    @Column(name = "nome", length = 64, nullable = false)
    private String nome;

    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @ManyToOne
    private Departamento departamento;
}
