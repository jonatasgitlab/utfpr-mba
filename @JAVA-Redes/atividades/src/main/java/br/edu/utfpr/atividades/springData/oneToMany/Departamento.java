package br.edu.utfpr.atividades.springData.oneToMany;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "DEPARTAMENTOS")
public class Departamento {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "nome", length = 64, nullable = false)
    private String nome;

    @OneToMany(targetEntity = Funcionario.class)
    private List<Funcionario> funcionarios;
}
