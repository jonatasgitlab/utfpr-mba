package br.edu.utfpr.atividades.springData.manyToOne;

import br.edu.utfpr.atividades.springData.oneToMany.Funcionario;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CLIENTES")
public class Cliente {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "nome", length = 64, nullable = false)
    private String nome;


}
