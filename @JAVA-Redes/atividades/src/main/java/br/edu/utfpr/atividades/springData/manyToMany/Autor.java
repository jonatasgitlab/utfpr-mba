package br.edu.utfpr.atividades.springData.manyToMany;

import br.edu.utfpr.atividades.springData.manyToOne.Cliente;

import javax.persistence.*;

@Entity
@Table(name = "AUTORES")
public class Autor {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "nome", length = 64, nullable = false)
    private String nome;

    @ManyToOne()
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;
}
