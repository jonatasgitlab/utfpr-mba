CREATE SCHEMA EMPXYZ;
USE EMPXYZ;

CREATE TABLE departamento (
    codDep integer NOT NULL auto_increment,
    nome character varying(50),
    CONSTRAINT departamento_pkey PRIMARY KEY (codDep)
);

CREATE TABLE funcionario (
    codFunc integer NOT NULL auto_increment,
    nome CHARACTER varying(50),
    qtdeDependentes INTEGER(11),
	 salario DECIMAL(15,2),
	 cargo CHARACTER varying(50),  
    codDep integer NOT NULL,
    CONSTRAINT funcionario_pkey PRIMARY KEY (codFunc),
    CONSTRAINT funcionario_codDep_fkey FOREIGN KEY (codDep) REFERENCES departamento(codDep)
);


INSERT INTO departamento (nome) VALUES ('Financeiro');
INSERT INTO departamento (nome) VALUES ('TI');
INSERT INTO departamento (nome) VALUES ('RH');
INSERT INTO departamento (nome) VALUES ('Administrativo');
INSERT INTO departamento (nome) VALUES ('Manutenção');
INSERT INTO departamento (nome) VALUES ('Diretoria Comercial');
INSERT INTO departamento (nome) VALUES ('Diretoria Operacional');

INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Abigail Celeste', 3, 6500.99, 'Coordenador(a)', 1);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Suelen Costa', 2, 9000.90, 'Coordenador(a)', 3);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Kleber Araujo', 2, 2500.00, 'Auxiliar', 5);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Rodrigo Setubal', 0, 6500.99, 'Coordenador(a)', 2);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Rodrigo Alexei', 3, 12000.00, 'Gerente', 2);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Cesar Galego', 2, 5800.50, 'Desenvolvedor(a)', 2);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Lucas Silva', 1, 6500.99, 'Engenheiro(a) de Software', 2);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Ketelin Figueira', 0, 2888.88, 'Analista', 4);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Suellen Costa', 0, 1500, 'Auxiliar', 4);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Emerson Dalilo', 2, 21000, 'Diretor(a)', 6);
INSERT INTO funcionario (nome, qtdeDependentes, salario, cargo, codDep) VALUES ('Daniel Gabs', 0, 15000, 'DIretor(a)', 7);

CREATE VIEW maior_departamento AS
SELECT d.nome AS nome_depto, COUNT(d.codDep) AS qtde_func FROM funcionario f
JOIN departamento d
ON f.codDep = d.codDep
GROUP BY d.nome
ORDER BY qtde_func DESC
LIMIT 1;

CREATE VIEW menor_departamento_sem_dependentes AS
SELECT d.nome AS nome_depto, COUNT(d.codDep) AS qtde_func FROM funcionario f
JOIN departamento d
ON f.codDep = d.codDep
WHERE f.qtdeDependentes = 0
GROUP BY d.nome
ORDER BY qtde_func asc
LIMIT 1;

CREATE VIEW departamentos_dir AS
SELECT d.nome AS nome_depto, f.nome FROM funcionario f
JOIN departamento d
ON f.codDep = d.codDep
having nome_depto LIKE 'DIR%'

CREATE VIEW func_maior_salario AS
SELECT f.nome AS nome_func, d.nome AS nome_depto FROM funcionario f
JOIN departamento d
ON f.codDep = d.codDep
ORDER BY f.salario DESC
LIMIT 1;

CREATE VIEW todos_gerentes AS
SELECT d.nome AS nome_depto, f.nome AS nome_func FROM funcionario f
JOIN departamento d
ON f.codDep = d.codDep
WHERE f.cargo LIKE 'Gerente';


CREATE USER 'funcionario'@'localhost' IDENTIFIED BY '123qwe';
CREATE USER 'gerente'@'localhost' IDENTIFIED BY '!123qwe';

GRANT ALL PRIVILEGES ON * . * TO 'gerente'@'localhost';
GRANT SELECT ON * . * TO 'funcionario'@'localhost';

