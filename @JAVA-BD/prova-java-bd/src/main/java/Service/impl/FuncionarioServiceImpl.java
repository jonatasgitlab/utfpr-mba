package Service.impl;

import Service.FuncionarioService;
import model.Cargo;
import model.Funcionario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repository.CargoRepository;
import repository.FuncionarioRepository;

@Service
public class FuncionarioServiceImpl implements FuncionarioService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private CargoRepository cargoRepository;

    @Override
    @Transactional
    public Integer salvarFuncionario() {
        Cargo carg1 = new Cargo();
        carg1.setCargo("Analista de Sistemas Pleno");
        cargoRepository.save(carg1);

        Funcionario func1 = new Funcionario();
        func1.setNome("George Russel da Massa");
        func1.setSexo("Masculino");
        func1.setTelefone("11982420455");
        func1.setCargo(carg1);

        Funcionario funcSalvo = funcionarioRepository.save(func1);

        return funcSalvo.getNome().isBlank() ? 0 : 1;
    }

    @Override
    public Integer excluirFuncionario() {
        var func = funcionarioRepository.findById(1);
        funcionarioRepository.deleteById(func.get().getId());
        cargoRepository.deleteById(func.get().getCargo().getId());

        var funcDeletado = funcionarioRepository.findById(1);

        return funcDeletado.get().getNome().isBlank() ? 1 : 0;
    }
}
