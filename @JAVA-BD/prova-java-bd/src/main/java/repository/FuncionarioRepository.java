package repository;

import model.Funcionario;
import org.springframework.data.repository.CrudRepository;

public interface FuncionarioRepository  extends CrudRepository<Funcionario, Integer> {
}
