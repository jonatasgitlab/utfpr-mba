package br.edu.utfpr.atividades.model;

import javax.persistence.*;

@Entity
@Table(name = "MUSICA")
public class Musica {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer duracao;
    private String titulo;

    @OneToOne(mappedBy = "musica")
    private Gravacao gravacao;

    @OneToOne
    @JoinColumn(name = "cod_categoria")
    private Categoria categoria;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Gravacao getGravacao() {
        return gravacao;
    }

    public void setGravacao(Gravacao gravacao) {
        this.gravacao = gravacao;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Integer getDuracao() {
        return duracao;
    }

    public void setDuracao(Integer duracao) {
        this.duracao = duracao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
