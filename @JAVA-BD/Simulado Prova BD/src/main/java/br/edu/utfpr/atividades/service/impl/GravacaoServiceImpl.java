package br.edu.utfpr.atividades.service.impl;

import br.edu.utfpr.atividades.model.*;
import br.edu.utfpr.atividades.repository.*;
import br.edu.utfpr.atividades.service.GravacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
public class GravacaoServiceImpl implements GravacaoService {
    @Autowired
    private GravacaoRepository gravacaoRepository;
    @Autowired
    private CantorRepository cantorRepository;
    @Autowired
    private GravadoraRepository gravadoraRepository;
    @Autowired
    private MusicaRepository musicaRepository;
    @Autowired
    private CategoriaRepository categoriaRepository;

    @Override
    @Transactional
    public Integer salvarGravacao() {
        Categoria cat = new Categoria();
        cat.setDescCategoria("Rock");
        categoriaRepository.save(cat);

        Musica musica = new Musica();
        musica.setCategoria(cat);
        musica.setDuracao(120);
        musica.setTitulo("Master of puppets");
        musicaRepository.save(musica);

        Gravadora gravadora = new Gravadora();
        gravadora.setNomeGravadora("Sony Music");
        gravadora.setPais("EUA");
        gravadoraRepository.save(gravadora);

        Cantor cantor = new Cantor();
        cantor.setNome("Mettalica");
        cantor.setPais("EUA");
        cantorRepository.save(cantor);

        Gravacao gravacao = new Gravacao(gravadora, cantor, musica, LocalDate.now());
        final var grav = gravacaoRepository.save(gravacao);


        return grav.getId();
    }

    @Override
    @Transactional
    public Integer excluirGravacao() {
        var gravacao = gravacaoRepository.findById(5);
        gravacaoRepository.delete(gravacao.get());
        final var cantor = cantorRepository.findById(4);
        cantorRepository.delete(cantor.get());
        gravadoraRepository.delete(gravadoraRepository.findById(3).get());

        musicaRepository.delete(musicaRepository.findById(2).get());

        return 1;
    }

    @Override
    public Gravacao consultarGravacao() {
        var gravacao = gravacaoRepository.findById(5);
        return gravacao.get();
    }
}
