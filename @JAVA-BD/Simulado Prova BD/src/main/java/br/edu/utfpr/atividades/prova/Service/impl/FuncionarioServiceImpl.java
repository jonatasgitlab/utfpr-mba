package br.edu.utfpr.atividades.prova.Service.impl;


import br.edu.utfpr.atividades.CommandLine;
import br.edu.utfpr.atividades.prova.Service.FuncionarioService;
import br.edu.utfpr.atividades.prova.model.Cargo;
import br.edu.utfpr.atividades.prova.model.Funcionario;
import br.edu.utfpr.atividades.prova.repository.CargoRepository;
import br.edu.utfpr.atividades.prova.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


@Service
public class FuncionarioServiceImpl implements FuncionarioService {

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    @Autowired
    private CargoRepository cargoRepository;

    @Override
    @Transactional
    public Integer salvarFuncionario() {
        Cargo carg1 = new Cargo();
        carg1.setCargo("Analista de Sistemas Junior");
        cargoRepository.save(carg1);

        Funcionario func1 = new Funcionario();
        func1.setNome("George Russel da Massa");
        func1.setSexo("Masculino");
        func1.setTelefone("11982420455");
        func1.setCargo(carg1);

        Cargo carg2 = new Cargo();
        carg2.setCargo("Analista de Sistemas Pleno");
        cargoRepository.save(carg2);

        Funcionario func2 = new Funcionario();
        func2.setNome("Mazespin da Silva");
        func2.setSexo("Masculino");
        func2.setTelefone("11982429999");
        func2.setCargo(carg2);

        Cargo carg3 = new Cargo();
        carg3.setCargo("Analista de Sistemas Senior");
        cargoRepository.save(carg3);

        Funcionario func3 = new Funcionario();
        func3.setNome("VAlteri Bottas");
        func3.setSexo("Masculino");
        func3.setTelefone("1191324657");
        func3.setCargo(carg3);

        Funcionario funcSalvo = funcionarioRepository.save(func1);
        funcionarioRepository.save(func2);
        funcionarioRepository.save(func3);

        return funcSalvo.getNome().isBlank() ? 0 : 1;
    }

    @Override
    public Integer excluirFuncionario() {
        var func = funcionarioRepository.findById(4);
        funcionarioRepository.deleteById(func.get().getId());
        cargoRepository.deleteById(func.get().getCargo().getId());

        var funcDeletado = funcionarioRepository.findById(4);

        return funcDeletado == null ? 0 : 1;
    }

    public List<Funcionario> buscarFuncionarios(){
        List<Funcionario> funcs = new ArrayList<>();
        funcionarioRepository.findAll().forEach(funcs::add);

        return funcs;
    }

    public List<Cargo> buscarCargos(){
        List<Cargo> cargos = new ArrayList<>();
        cargoRepository.findAll().forEach(cargos::add);

        return cargos;
    }

    public List<String> buscarFuncionariosOrdemAlfa(){
        List<String> nomes = new ArrayList<>();
        List<Funcionario> funcs = new ArrayList<>();
        funcionarioRepository.findAll().forEach(funcionario -> nomes.add(funcionario.getNome()));

        nomes.stream()
                .sorted(Comparator.naturalOrder());

        return nomes;
    }

    public Integer buscarQuantidadeFuncs(){
        List<Funcionario> funcs = new ArrayList<>();
        funcionarioRepository.findAll().forEach(funcs::add);

        return funcs.size();
    }

}
