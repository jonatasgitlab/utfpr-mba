package br.edu.utfpr.atividades.repository;

import br.edu.utfpr.atividades.model.Pessoa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PessoaRepository extends CrudRepository<Pessoa, Integer> {
}
