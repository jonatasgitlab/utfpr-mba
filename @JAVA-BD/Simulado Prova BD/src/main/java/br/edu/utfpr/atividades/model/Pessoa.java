package br.edu.utfpr.atividades.model;

import javax.persistence.*;

@Entity
@Table(name = "PESSOA")
public class Pessoa {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String nomePessoa;

    @OneToOne(mappedBy = "pessoa")
    private Fone fone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public Fone getFone() {
        return fone;
    }

    public void setFone(Fone fone) {
        this.fone = fone;
    }

    public Pessoa(Integer id, String nomePessoa, Fone fone) {
        this.id = id;
        this.nomePessoa = nomePessoa;
        this.fone = fone;
    }
}
