package br.edu.utfpr.atividades.repository;

import br.edu.utfpr.atividades.model.Cantor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CantorRepository  extends CrudRepository<Cantor, Integer> {
}
