package br.edu.utfpr.atividades.prova.Service;

public interface FuncionarioService {
    Integer salvarFuncionario();
    Integer excluirFuncionario();
}
