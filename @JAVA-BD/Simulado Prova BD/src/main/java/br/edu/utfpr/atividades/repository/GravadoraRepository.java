package br.edu.utfpr.atividades.repository;

import br.edu.utfpr.atividades.model.Gravadora;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GravadoraRepository extends CrudRepository<Gravadora, Integer> {
}
