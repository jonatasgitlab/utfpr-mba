package br.edu.utfpr.atividades.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "GRAVACAO")
public class Gravacao {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "cod_gravadora")
    private Gravadora gravadora;

    @OneToOne
    @JoinColumn(name = "cod_cantor")
    private Cantor cantor;

    @OneToOne
    @JoinColumn(name = "cod_musica")
    private Musica musica;

    private LocalDate dataGravacao;

    public Gravacao() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Gravadora getGravadora() {
        return gravadora;
    }

    public void setGravadora(Gravadora gravadora) {
        this.gravadora = gravadora;
    }

    public Cantor getCantor() {
        return cantor;
    }

    public void setCantor(Cantor cantor) {
        this.cantor = cantor;
    }

    public Musica getMusica() {
        return musica;
    }

    public void setMusica(Musica musica) {
        this.musica = musica;
    }

    public LocalDate getDataGravacao() {
        return dataGravacao;
    }

    public void setDataGravacao(LocalDate dataGravacao) {
        this.dataGravacao = dataGravacao;
    }

    public Gravacao(Gravadora gravadora, Cantor cantor, Musica musica, LocalDate dataGravacao) {
        this.gravadora = gravadora;
        this.cantor = cantor;
        this.musica = musica;
        this.dataGravacao = dataGravacao;
    }
}