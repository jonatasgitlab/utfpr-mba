package br.edu.utfpr.atividades.prova.repository;


import br.edu.utfpr.atividades.prova.model.Cargo;
import org.springframework.data.repository.CrudRepository;

public interface CargoRepository extends CrudRepository<Cargo, Integer> {
}
