package br.edu.utfpr.atividades.prova.repository;


import br.edu.utfpr.atividades.prova.model.Funcionario;
import org.springframework.data.repository.CrudRepository;

public interface FuncionarioRepository  extends CrudRepository<Funcionario, Integer> {
}
