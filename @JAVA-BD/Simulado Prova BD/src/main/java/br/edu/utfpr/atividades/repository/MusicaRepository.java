package br.edu.utfpr.atividades.repository;

import br.edu.utfpr.atividades.model.Musica;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MusicaRepository extends CrudRepository<Musica, Integer> {
}
