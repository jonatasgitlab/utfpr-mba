package br.edu.utfpr.atividades.repository;

import br.edu.utfpr.atividades.model.Fone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FoneRepository extends CrudRepository<Fone, Integer> {
}
