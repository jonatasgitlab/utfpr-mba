package br.edu.utfpr.atividades.service;

import br.edu.utfpr.atividades.model.Gravacao;
import org.springframework.stereotype.Service;


public interface GravacaoService {
    Integer salvarGravacao();
    Integer excluirGravacao();
    Gravacao consultarGravacao();
}
