package br.edu.utfpr.atividades.model;

import javax.persistence.*;

@Entity
@Table(name = "GRAVADORA")
public class Gravadora {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String nomeGravadora;
    private String pais;

    @OneToOne(mappedBy = "gravadora")
    private Gravacao gravacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeGravadora() {
        return nomeGravadora;
    }

    public void setNomeGravadora(String nomeGravadora) {
        this.nomeGravadora = nomeGravadora;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Gravacao getGravacao() {
        return gravacao;
    }

    public void setGravacao(Gravacao gravacao) {
        this.gravacao = gravacao;
    }

}
