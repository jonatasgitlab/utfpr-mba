package br.edu.utfpr.atividades;

import br.edu.utfpr.atividades.prova.Service.impl.FuncionarioServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandLine implements CommandLineRunner {

    @Autowired
    private FuncionarioServiceImpl funcionarioService;

    @Override
    public void run(String... args) throws Exception {

        System.out.println("Funcionarios Incluídos, retorno = " + funcionarioService.salvarFuncionario());

        System.out.println("Apagando Funcionario " + funcionarioService.excluirFuncionario());

        System.out.println("Listando Todos Funcs.....................");
        funcionarioService.buscarFuncionarios().stream()
                .forEach(func -> System.out.println(func.getNome() + " Cargo: " + func.getCargo().getCargo()));

        System.out.println("Listando Todos Cargos.....................");
        funcionarioService.buscarCargos().stream()
                .forEach(cargo -> System.out.println(cargo.getCargo() + " Func: " + cargo.getFuncionario().getNome()));

        System.out.println("Listando todos nomes em ordem......................");
        funcionarioService.buscarFuncionariosOrdemAlfa().stream()
                .forEach(System.out::println);

        System.out.println("Listando total de funcionarios......................");
        System.out.println(funcionarioService.buscarQuantidadeFuncs());
    }
}
