package br.edu.utfpr.atividades.repository;

import br.edu.utfpr.atividades.model.Gravacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GravacaoRepository extends CrudRepository<Gravacao, Integer> {
}
