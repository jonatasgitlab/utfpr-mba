import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Cliente {
    public static void main(String[] args) {
        try {
            Registry registro = LocateRegistry.getRegistry("localhost", 1099);
            Calculadora stub  = (Calculadora) registro.lookup("metodosCalculadora");

            System.out.println("Nova soma de 25 + 7");
            System.out.println("resultado é: " + stub.add(25, 7));

            System.out.println("Nova subtração de 11 - 7");
            System.out.println("resultado é: " + stub.sub(11, 7));

            System.out.println("Nova Multiplicação de 3 * 7");
            System.out.println("resultado é: " + stub.mul(3, 7));

            System.out.println("Nova subtração de 28 / 7");
            System.out.println("resultado é: " + stub.div(28, 7));

        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }
}
