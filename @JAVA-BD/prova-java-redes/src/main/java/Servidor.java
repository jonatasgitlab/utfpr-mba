import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Servidor  implements  Calculadora{
    @Override
    public long add(long a, long b) throws RemoteException {
        var soma = a + b;
        return soma;
    }

    @Override
    public long sub(long a, long b) throws RemoteException {
        var subtracao = a - b;
        return subtracao;
    }

    @Override
    public long mul(long a, long b) throws RemoteException {
        var multiplicacao = a * b;
        return multiplicacao;
    }

    @Override
    public long div(long a, long b) throws RemoteException {
        var divisao = a / b;
        return divisao;
    }

    public static void main(String[] args) throws RemoteException, AlreadyBoundException {
        Servidor s = new Servidor();
        Calculadora stub = (Calculadora) UnicastRemoteObject.exportObject(s, 0);
        Registry registro = LocateRegistry.getRegistry("localhost", 1099);

        registro.bind("metodosCalculadora", stub);

        System.out.println("Servidor Calculadora Prnto");
    }
}
