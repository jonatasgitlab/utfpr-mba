import java.util.Arrays;

public enum TipoContaEnum {
    CACC(1),
    POUP(2),
    SALIS(3),
    DIGIT(4);

    private int codigo;

    TipoContaEnum(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }

    public static TipoContaEnum getTipoByCodigo(int codigo) {
        var tipo = Arrays.stream(TipoContaEnum.values()).filter(type -> type.getCodigo() == codigo).findFirst().get();
        return tipo;
    }
}
