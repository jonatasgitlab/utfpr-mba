package br.edu.utfpr.atividades.atv05.repository;

import br.edu.utfpr.atividades.atv05.model.Departamento;
import br.edu.utfpr.atividades.atv05.model.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Integer> {
    List<Funcionario> findByNameAndQtdeDependentes(String name, Integer qtdeDependentes);
    List<Funcionario> findFirstByOrderBySalarioDesc();
    List<Funcionario> findTop3ByOrderBySalarioDesc();


    @Query("SELECT f FROM Funcionario f WHERE f.departamento = :dep")
    List<Funcionario> findAllFuncionariosByDepartamento(@Param("dep") Optional<Departamento> dep);

    @Query("SELECT f from Funcionario f WHERE f.qtdeDependentes = 0 ORDER BY f.name")
    List<Funcionario> findFuncionariosSemDependentesAsc();

    @Query("SELECT f from Funcionario f WHERE f.salario > 100 ORDER BY f.salario")
    List<Funcionario> findFuncionariosComSalarioMaiorQue100();

    @Query(value = "SELECT * from FUNCIONARIOS f WHERE f.salario > :val ORDER BY f.salario", nativeQuery = true)
    List<Funcionario> findFuncionariosComSalarioMaiorQue(@Param("val") Double val);

    List<Funcionario> findByDependentes(@Param("qtde") Integer qtde);
    List<Funcionario> findByQualquerNome(@Param("nome") String nome);

    @Procedure(procedureName = "procedure_aumento_salario")
    Integer proceduraAumento(Integer arg);

    @Query("SELECT f from Funcionario f WHERE f.departamento = :dep AND f.qtdeDependentes = 0 ORDER BY f.name")
    List<Funcionario> findFuncionariosPorDeptoSemDependentesAsc(@Param("dep") Optional<Departamento> dep);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Funcionario f SET f.departamento = :dep")
    int updateDeptoFuncionarios(@Param("dep") Optional<Departamento> dep);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("DELETE Funcionario f where f.departamento = :dep")
    int deleteFuncFromDepto(@Param("dep") Optional<Departamento> dep);
}
