package br.edu.utfpr.atividades.atv05;

import br.edu.utfpr.atividades.atv05.model.Departamento;
import br.edu.utfpr.atividades.atv05.model.Funcionario;
import br.edu.utfpr.atividades.atv05.repository.DepartamentoRepository;
import br.edu.utfpr.atividades.atv05.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DepartamentoService {
    @Autowired
    DepartamentoRepository deptoRepo;

    @Autowired
    FuncionarioRepository funcRepo;

    @Transactional(readOnly = false)
    public Funcionario SalvarNovoFunc(Funcionario func, Departamento depto){
        deptoRepo.save(depto);
        func.setDepartamento(depto);
        return funcRepo.save(func);
    }
}
