package br.edu.utfpr.atividades.atv05.repository;

import br.edu.utfpr.atividades.atv05.model.Departamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartamentoRepository extends CrudRepository<Departamento, Integer> {
}
