package br.edu.utfpr.atividades.atv05.controller;

import br.edu.utfpr.atividades.atv05.DepartamentoService;
import br.edu.utfpr.atividades.atv05.model.Departamento;
import br.edu.utfpr.atividades.atv05.model.Funcionario;
import br.edu.utfpr.atividades.atv05.repository.DepartamentoRepository;
import br.edu.utfpr.atividades.atv05.repository.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
public class TestDBontroller {

    @Autowired
    DepartamentoRepository departamentoRepo;

    @Autowired
    FuncionarioRepository funcRepo;

    @Autowired
    DepartamentoService deptoService;

    @GetMapping("/addDepartamento")
    public ResponseEntity<Departamento> addNewDepartamento(@RequestParam String nome){
        Departamento dep1 = new Departamento();
        dep1.setName(nome);
        Departamento result = departamentoRepo.save(dep1);
        return new ResponseEntity<Departamento>(result, HttpStatus.OK);
    }

    @GetMapping("/addFuncionario")
    public ResponseEntity<Funcionario> addFuncionario(String nome, Integer dependentes){
        Departamento dep1 = new Departamento();
        dep1.setName("TI");
        departamentoRepo.save(dep1);

        Funcionario func = new Funcionario();
        Departamento dep = new Departamento();
        dep = departamentoRepo.findById(1).get();
        func.setName(nome);//+ new Random().nextInt());
        func.setDepartamento( dep);
        func.setCarga("Analista");
        func.setSalario(new Random().nextDouble()*1000);
        func.setQtdeDependentes(dependentes);
        Funcionario result = funcRepo.save(func);

        return new ResponseEntity<Funcionario>(result, HttpStatus.OK);
    }
    @GetMapping("/atividade06")
    public HashMap<String, String> atividade06(){
        this.addFuncionario("Bob Martin", 2);
        HashMap<String, String> retorno = new HashMap<String, String>();

        List<Funcionario> func = funcRepo.findByNameAndQtdeDependentes("Bob Martin", 2);
        retorno.put("QUESTAO-01 ",  func.get(0).getName() + "|| qtde Dependentes "  + func.get(0).getQtdeDependentes());

        this.addFuncionario("Sergio Lopez", 2);
        List<Funcionario> funcs = funcRepo.findAllFuncionariosByDepartamento(departamentoRepo.findById(1));
        retorno.put("QUESTAO-02 ", funcs.get(0).getName()  + " | " + funcs.get(1).getName());

        Optional<Departamento> depto = departamentoRepo.findById(1);
        retorno.put("QUESTAO-03 ", depto.get().getName());

        List<Funcionario> maioresSalarios = funcRepo.findFirstByOrderBySalarioDesc(); //funcRepo.findAll(Sort.by("salario"));
        retorno.put("QUESTAO-04 ", maioresSalarios.get(0).getName() + maioresSalarios.get(0).getSalario());

        this.addFuncionario("Robertinha Lemos",0);
        List<Funcionario> top3Salarios = funcRepo.findTop3ByOrderBySalarioDesc();
        retorno.put("QUESTAO-05 ", top3Salarios.get(0).getName()  + " | " + top3Salarios.get(1).getName() + " | " + top3Salarios.get(2).getName());

        List<Funcionario> semDependentes = funcRepo.findFuncionariosSemDependentesAsc();
        retorno.put("QUESTAO-06 ", "Numero de Funcionarios sem dependentes " + semDependentes.size());

        List<Funcionario> salarioMaior100 = funcRepo.findFuncionariosComSalarioMaiorQue100();
        retorno.put("QUESTAO-07 ", "Numero de Funcionarios com salario maior que 100 " + salarioMaior100.size());

        List<Funcionario> salarioMaiorQUe = funcRepo.findFuncionariosComSalarioMaiorQue(250.00);
        retorno.put("QUESTAO-08 ", "Numero de Funcionarios com salario maior que PARAM (NATIVE) " + salarioMaiorQUe.size());

        List<Funcionario> porDependentesNamedQuery = funcRepo.findByDependentes(2);
        retorno.put("QUESTAO-09 ", "Numero de Funcionarios com param Dependentes NamedQuery " + porDependentesNamedQuery.size());

        List<Funcionario> porQualquerNome = funcRepo.findByQualquerNome("bob");
        retorno.put("QUESTAO-10 ", "Native Named Query busca por qualquer nome " + porQualquerNome.size());
        System.out.println(retorno);


        return retorno;
    }

    @GetMapping("/atividade07")
    public Integer atividade07 (){
        //QUESTAO-01  aumento de 10%
         funcRepo.proceduraAumento(10);

        //QUESTAO-02
        List<Funcionario> funcsSemDependente = funcRepo.findFuncionariosPorDeptoSemDependentesAsc(departamentoRepo.findById(1));
        for(Funcionario func : funcsSemDependente){
            System.out.println(func.getName());
            System.out.println(func.getDepartamento().getName());
        }
        //Questao-04
        Integer ret1 = funcRepo.deleteFuncFromDepto(departamentoRepo.findById(13));

        //QUESTAO-03
        Integer ret =  funcRepo.updateDeptoFuncionarios(departamentoRepo.findById(14));
        System.out.println(ret);


        return ret;
    }

    @GetMapping("/atividade08")
    public Funcionario atividade08(){
        Departamento depto = new Departamento();
        depto.setName("Logistica");

        Funcionario func = new Funcionario();
        func.setName("José Logistics");
        func.setSalario(8500.55);
        func.setCarga("Gerente");
        func.setQtdeDependentes(5);

        return  deptoService.SalvarNovoFunc(func, depto);
    }


}
