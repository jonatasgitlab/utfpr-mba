package br.edu.utfpr.atividades.atv05.model;

import com.fasterxml.jackson.databind.deser.impl.CreatorCandidate;

import javax.persistence.*;

@Entity
@Table(name = "FUNCIONARIOS")
@NamedQuery(name = "Funcionario.findByDependentes", query = "SELECT f FROM Funcionario f WHERE f.qtdeDependentes = :qtde")
@NamedNativeQuery(name = "Funcionario.findByQualquerNome", query = "SELECT * FROM FUNCIONARIOS f WHERE f.name LIKE CONCAT('%',':nome','%')")
@NamedStoredProcedureQuery(
        name="Funcionario.aumento",
        procedureName = "procedure_aumento_salario",
        parameters = {
                @StoredProcedureParameter(
                        mode = ParameterMode.IN,
                        name = "arg",
                        type = Integer.class
                ),
                @StoredProcedureParameter(
                        mode = ParameterMode.OUT,
                        name = "res",
                        type = Integer.class
                )
        }
)
public class Funcionario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private Integer qtdeDependentes;
    private Double salario;
    private String Carga;

    @ManyToOne
    private Departamento departamento;

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQtdeDependentes() {
        return qtdeDependentes;
    }

    public void setQtdeDependentes(Integer qtdeDependentes) {
        this.qtdeDependentes = qtdeDependentes;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getCarga() {
        return Carga;
    }

    public void setCarga(String carga) {
        Carga = carga;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }
}
