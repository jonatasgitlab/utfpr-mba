USE dml;

#RESPOSTAS

#Questao 01
SELECT 
C.nome_cantor
, COUNT(G.cod_gravacao) AS num_gravacoes
FROM cantor C
JOIN gravacao G ON C.cod_cantor = G.cod_cantor
GROUP BY C.nome_cantor
HAVING COUNT(G.cod_gravacao) = (
SELECT MIN(num_gravacoes)
FROM
(
SELECT 
C.nome_cantor
, COUNT(G.cod_gravacao) AS num_gravacoes
FROM cantor C
JOIN gravacao G ON C.cod_cantor = G.cod_cantor
GROUP BY C.nome_cantor
#HAVING (COUNT(G.cod_gravacao) = 1)
) AS tmp
)

#Questao 02
SELECT cantor, COUNT(cantor)
FROM 
(SELECT C.nome_cantor AS cantor, r.nome_gravadora AS gravadora
FROM cantor C 
JOIN GRAVACAO G 
ON C.COD_CANTOR = G.COD_CANTOR
JOIN gravadora R
ON G.cod_gravadora = R.cod_gravadora
GROUP BY C.nome_cantor, r.nome_gravadora
ORDER BY C.nome_cantor) AS tmp
GROUP BY cantor
HAVING COUNT(cantor) = (
SELECT MAX(qtde)
from
(SELECT cantor, COUNT(cantor) AS qtde
FROM 
(SELECT C.nome_cantor AS cantor, r.nome_gravadora AS gravadora
FROM cantor C 
JOIN GRAVACAO G 
ON C.COD_CANTOR = G.COD_CANTOR
JOIN gravadora R
ON G.cod_gravadora = R.cod_gravadora
GROUP BY C.nome_cantor, r.nome_gravadora
ORDER BY C.nome_cantor) AS tmp
GROUP BY cantor) AS tmp2
)

#Questao 3
SELECT cantor, AVG(dur)
FROM
(
SELECT C.nome_cantor AS cantor, M.duracao AS dur
FROM cantor C
JOIN gravacao G
ON C.COD_CANTOR = G.cod_cantor
JOIN musica M
ON G.COD_MUSICA = M.COD_MUSICA
) AS tmp
GROUP BY cantor 
ORDER BY AVG(dur) DESC
LIMIT 1

#Questao 04
SELECT C.nome_cantor cantor, R.nome_gravadora gravadora
FROM cantor C
LEFT join gravacao G
ON C.cod_cantor = G.cod_cantor
LEFT JOIN gravadora R
ON G.cod_gravadora = R.cod_gravadora
GROUP BY cantor
HAVING gravadora NOT LIKE 'Sony' OR gravadora IS NULL
ORDER BY cantor

#Questao 05
SELECT C.nome_cantor cantor, M.titulo musica, G.data_gravacao
FROM cantor C
JOIN gravacao G
ON C.cod_cantor =  G.cod_cantor
JOIN musica M
ON G.cod_musica = M.cod_musica
WHERE YEAR(G.data_gravacao) = 2004

#Questao 06
SELECT C.nome_cantor cantor, MAX(G.data_gravacao) AS data_ultima_gravacao
FROM cantor C
LEFT JOIN gravacao G
ON C.cod_cantor =  G.cod_cantor
GROUP BY cantor
ORDER BY data_ultima_gravacao DESC 

#Questao 07
SELECT P.nome_pessoa, F.numero AS fone_residencial, F2.numero AS fone_comercial, F3.numero AS celular
FROM pessoa P
JOIN fone F
ON P.cod_pessoa = F.cod_pessoa
JOIN fone F2
ON P.cod_pessoa = F2.cod_pessoa
JOIN fone  F3
ON P.cod_pessoa = F3.cod_pessoa
WHERE F.tipo = 'R' AND F2.tipo = 'C' AND F3.tipo = 'L'



