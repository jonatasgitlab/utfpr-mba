import javax.persistence.*;

@Entity
@Table(name = "FUNCIONARIOS")
public class Funcionario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private Integer qtdeDependentes;
    private Double salario;
    private String Carga;

    @OneToOne
    private Departamento departamento;
}
